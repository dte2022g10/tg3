import redis

r = redis.Redis(
    host = 'redis-14432.c277.us-east-1-3.ec2.cloud.redislabs.com',
    port = 14432,
    password= 'DfoarklLRTgq67s4aDBOs7IZ0p5CCyb8',
    decode_responses=True
)

def poblado_inicial():
    r.hset("dte1",'Nombre',"Ignacio")
    r.hset("dte1",'Apellido',"López")
    r.hset("dte1", 'dni','12345678A')
    r.hset("dte1", 'Funcion','Desarrollo del prototipo virtual de redis')

    r.hset("dte2",'Nombre',"Enrique")
    r.hset("dte2",'Apellido',"Rodríguez")
    r.hset("dte2", 'dni','12345678B')
    r.hset("dte2", 'Funcion','Criterios de comparacion')

    r.hset("dte3",'Nombre',"Francisco")
    r.hset("dte3",'Apellido',"Bravo")
    r.hset("dte3", 'dni','12345678C')
    r.hset("dte3", 'Funcion','Comparacion de los prototipos')

    r.hset("dte4",'Nombre',"Pablo")
    r.hset("dte4",'Apellido',"Contreras")
    r.hset("dte4", 'dni','12345678D')
    r.hset("dte4", 'Funcion','Lider, información general,requisitos y conclusiones')

    r.hset("dte5",'Nombre',"Guillermo")
    r.hset("dte5",'Apellido',"San Andres")
    r.hset("dte5", 'dni','12345678E')
    r.hset("dte5", 'Funcion','Prototipo kafka')

    r.hset("profesor1",'Nombre',"Manuel")
    r.hset("profesor1",'Apellido',"Buenaga")
    r.hset("profesor1",'Asignaturas',"dte")
  


def crear_clave(tipo):
    clave = tipo +"1"
    existencia = r.exists(clave)
    n = 2

    while existencia == 1:
        i = str(n)
        clave = tipo + i
        existencia = r.exists(clave)
        n+=1
    
    return(clave)

def agregar_alumno(asignatura,nombre,apellido,dni,funcion):
    
    clave = crear_clave(asignatura)
    r.hset(clave,'Nombre',nombre)
    r.hset(clave,'Apellido',apellido)
    r.hset(clave, 'dni',dni)
    r.hset(clave, 'Funcion',funcion)
    
    print("Alumno agregado con éxito")
    print(r.hgetall(clave))

def agregar_profesor(nombre,apellido,asignatura):
    
    clave = crear_clave('profesor')
    r.hset(clave,'Nombre',nombre)
    r.hset(clave,'Apellido',apellido)
    r.hset(clave, 'Asignaturas',asignatura)
    
    print("Profesor agregado con éxito")
    print(r.hgetall(clave))

def imprimir_usuario(tipo):
    clave = tipo +'1'
    n = 1
    while r.exists(clave):
        print('clave: ',clave,r.hgetall(clave))
        n+=1
        i = str(n)
        clave = tipo+i

def eliminar_usuario(clave):
    r.delete(clave)
    print("Usuario eliminado con éxito")


def menu():
    continuar = True
    while continuar:
        print()
        print("1 para agregar alumno")
        print("2 para agregar profesor")
        print("3 para ver alumnos")
        print("4 para ver profesores")
        print("5 para eliminar un usuario")
        print("6 para terminar")
        accion = input()
        if(accion =="1"):
            asignatura = input("Asignatura: ")
            nombre = input("nombre: ")
            apellido = input("apellido: ")
            dni = input("dni: ")
            funcion = input("funcion que desempeñas: ")
            agregar_alumno(asignatura,nombre,apellido,dni,funcion)
        elif(accion =="2"):
            nombre = input("nombre: ")
            apellido = input("apellido: ")
            asignatura = input("asignatura: ")
            agregar_profesor(nombre,apellido,asignatura)
        elif(accion =="3"):
            imprimir_usuario("dte")
        elif(accion =="4"):
            imprimir_usuario("profesor")
        elif(accion=="5"):
            clave = input("Clave del usuairo a eliminar: ")
            eliminar_usuario(clave)
        elif(accion=="6"):
            continuar = False
            print("GRACIAS POR UTILIZAR EL PROTOTIPO REDIS :)")
        else:
            print("Accion",accion," no disponible")

#poblado_inicial() #utlizar para realizar un poblado inicial de prueba
menu()